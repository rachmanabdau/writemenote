package com.volvoxglobator.writemenote.util

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.volvoxglobator.writemenote.database.Note
import com.volvoxglobator.writemenote.databinding.RecyclerviewItemBinding

class RecyclerViewAdapter(private val onClickListener: OnClickListener) :
    ListAdapter<Note, CustomViewHolder>(NoteDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        return CustomViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val notes = getItem(position)

        holder.bind(notes)

        // Handle tapped note
        holder.itemView.setOnClickListener {
            onClickListener.clickListener(notes)
        }
    }
}

class CustomViewHolder(val binding: RecyclerviewItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    companion object {
        fun from(parent: ViewGroup): CustomViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = RecyclerviewItemBinding.inflate(layoutInflater, parent, false)

            return CustomViewHolder(binding)
        }
    }

    fun bind(notes: Note) {
        binding.noteData = notes
        binding.executePendingBindings()
    }
}

class OnClickListener(val clickListener: (note: Note) -> Unit)
class OnLongClickListener(val clickListener: (note: Note) -> Boolean)

class NoteDiffCallback : DiffUtil.ItemCallback<Note>() {
    override fun areItemsTheSame(oldItem: Note, newItem: Note): Boolean {
        return oldItem.noteId == newItem.noteId
    }

    override fun areContentsTheSame(oldItem: Note, newItem: Note): Boolean {
        return oldItem == newItem
    }

}