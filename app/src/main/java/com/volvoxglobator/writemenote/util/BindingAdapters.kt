package com.volvoxglobator.writemenote.util

import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.volvoxglobator.writemenote.database.Note
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.FormatStyle

@BindingAdapter("noteTitle")
fun bindNoteTitle(itemTextView: TextView, note: Note) {
    itemTextView.text = note.title
}

@BindingAdapter("noteDescription")
fun bindNoteDescription(itemTextView: TextView, note: Note) {
    itemTextView.text = note.description
}

@BindingAdapter("noteDateTime")
fun bindNoteDateTime(itemTextView: TextView, note: Note) {
    val dateTimeString = note.dateCreated!!.format(
        DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL, FormatStyle.MEDIUM)
    )
    itemTextView.text = dateTimeString
}

@BindingAdapter("noteList")
fun bindNoteList(recyclerView: RecyclerView, notes: List<Note>?){
    val adapter = recyclerView.adapter as RecyclerViewAdapter
    adapter.submitList(notes)
}