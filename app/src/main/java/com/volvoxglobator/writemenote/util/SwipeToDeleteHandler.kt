package com.volvoxglobator.writemenote.util

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.volvoxglobator.writemenote.R

class SwipeToDeleteHandler(context: Context, private val onDelete: (CustomViewHolder) -> Unit) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT or ItemTouchHelper.LEFT) {

    private val background = ColorDrawable(Color.RED)
    private val deleteIcon = ContextCompat.getDrawable(context, R.drawable.ic_delete)
    private val iconMargin = 24

    override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        viewHolder.let {
            val listItemViewHolder = viewHolder as CustomViewHolder
            onDelete(listItemViewHolder)
        }
    }

    override fun onChildDraw(
            c: Canvas,
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            dX: Float,
            dY: Float,
            actionState: Int,
            isCurrentlyActive: Boolean
    ) {
        if (viewHolder.adapterPosition < 0) return

        val item = viewHolder.itemView // the view being swiped

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            // Draw background and icon when user swipe to the left
            if (dX < 0) {
                // draw the red background, based on the offset of the swipe (dX)
                background.apply {
                    setBounds(item.right + dX.toInt(), item.top, item.right, item.bottom)
                    draw(c)
                }

                // draw the symbol
                deleteIcon?.apply {
                    val xt = item.top + (item.bottom - item.top - deleteIcon.intrinsicHeight) / 2
                    setBounds(
                            item.right - iconMargin - deleteIcon.intrinsicWidth,
                            xt,
                            item.right - iconMargin,
                            xt + deleteIcon.intrinsicHeight
                    )
                    draw(c)
                }
            } else {
                // Draw background and icon when user swipe to the right

                // draw the red background, based on the offset of the swipe (dX)
                background.apply {
                    setBounds(item.left, item.top, item.left + dX.toInt(), item.bottom)
                    draw(c)
                }

                // draw the symbol
                deleteIcon?.apply {
                    val xt = item.top + (item.bottom - item.top - deleteIcon.intrinsicHeight) / 2
                    setBounds(
                            item.left + iconMargin,
                            xt,
                            item.left + iconMargin + deleteIcon.intrinsicWidth,
                            xt + deleteIcon.intrinsicHeight
                    )
                    draw(c)
                }
            }
        }

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }

}