package com.volvoxglobator.writemenote.util

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.pranavpandey.android.dynamic.toasts.DynamicToast


fun hideVirtualKeyBoard(context: Context, view: View) {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun showSuccessToast(context: Context, message: String) {
    DynamicToast.makeSuccess(context, message).show()
}