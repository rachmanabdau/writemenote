package com.volvoxglobator.writemenote.notelist


import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.volvoxglobator.writemenote.R
import com.volvoxglobator.writemenote.database.NoteDatabase
import com.volvoxglobator.writemenote.databinding.FragmentNoteListBinding
import com.volvoxglobator.writemenote.util.OnClickListener
import com.volvoxglobator.writemenote.util.RecyclerViewAdapter
import com.volvoxglobator.writemenote.util.SwipeToDeleteHandler

/**
 * A simple [Fragment] subclass.
 *
 */
class NoteListFragment : Fragment() {

    private lateinit var noteViewModel: NoteListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = FragmentNoteListBinding.inflate(inflater)

        val application = requireNotNull(this.activity).application
        val dataSource = NoteDatabase.getInstance(application).noteDatabaseDao
        val viewModelFactory = NoteListViewModelFactory(dataSource, application)
        noteViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(NoteListViewModel::class.java)

        val adapter = RecyclerViewAdapter(
            // When a note is tapped, pass id, title and description to edit note fragment
            OnClickListener {
                this.requireView().findNavController().navigate(
                    NoteListFragmentDirections.actionNoteListFragmentToEditNoteFragment(
                        it.noteId, it.title, it.description
                    )
                )
            })

        binding.setLifecycleOwner(this)
        binding.viewModel = noteViewModel
        binding.recyclerviewNoteList.layoutManager = LinearLayoutManager(this.requireContext())
        binding.recyclerviewNoteList.adapter = adapter

        val itemTouchHelper = ItemTouchHelper(SwipeToDeleteHandler(this.requireContext()) {
            val data = it.binding.noteData
            data.let { noteViewModel.deleteNote(it!!) }

            val customSnackbar = Snackbar.make(
                this.requireView(),
                getString(R.string.delete_snackbar),
                Snackbar.LENGTH_SHORT
            )
            customSnackbar.setAction(getString(R.string.undo_snackbar), {
                noteViewModel.insertNote(data!!)
            })

            customSnackbar.show()
        })
        itemTouchHelper.attachToRecyclerView(binding.recyclerviewNoteList)
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.overflow_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item, view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }
}
