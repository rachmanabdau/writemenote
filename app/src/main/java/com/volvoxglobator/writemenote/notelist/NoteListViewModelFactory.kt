package com.volvoxglobator.writemenote.notelist

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.volvoxglobator.writemenote.database.NoteDatabaseDao

class NoteListViewModelFactory(
    private val database: NoteDatabaseDao,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NoteListViewModel::class.java)) {
            return NoteListViewModel(database, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}