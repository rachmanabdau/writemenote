package com.volvoxglobator.writemenote.notelist

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.volvoxglobator.writemenote.database.Note
import com.volvoxglobator.writemenote.database.NoteDatabaseDao
import kotlinx.coroutines.*

class NoteListViewModel(private val database: NoteDatabaseDao, application: Application)
    : AndroidViewModel(application) {

    private val viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    val allNote: LiveData<List<Note>>

    init {
        // Get all data from database
        allNote = database.getAllNote()
    }

    fun deleteNote(note: Note) {
        uiScope.launch {
            withContext(Dispatchers.IO) {
                database.delete(note)
            }
        }
    }

    fun insertNote(note: Note) {
        uiScope.launch {
            withContext(Dispatchers.IO) {
                database.insert(note)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}