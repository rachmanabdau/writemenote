package com.volvoxglobator.writemenote

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.preference.PreferenceManager
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.volvoxglobator.writemenote.databinding.ActivityMainBinding
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt
import uk.co.samuelwall.materialtaptargetprompt.extras.backgrounds.RectanglePromptBackground
import uk.co.samuelwall.materialtaptargetprompt.extras.focals.RectanglePromptFocal

class MainActivity : AppCompatActivity() {

    private lateinit var myNavController: NavController
    private var drawerLayout: DrawerLayout? = null
    private var navView: NavigationView? = null
    private lateinit var appBar: AppBarConfiguration
    private var bottomNavView: BottomNavigationView? = null
    private lateinit var toobar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        @Suppress("UNUSED_VARIABLE")
        val binding =
            DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)

        myNavController = Navigation.findNavController(this, R.id.myNavHostFragment)
        drawerLayout = binding.drawerLayout
        bottomNavView = binding.bottomNavigation
        navView = binding.navView

        applyNavigation()

        // Show prompt at the first time application running
        promptBottomNewNote()

        try {
            // Initialize AdMob
            MobileAds.initialize(this, "ca-app-pub-5598015846030268~7867818011")
            // Load Ad
            val adRequest = AdRequest.Builder().build()
            binding.adView.loadAd(adRequest)
        } catch (e: Throwable) {
            Log.e("LoadingAd", e.message.toString())
        }
    }

    private fun applyNavigation() {
        if (drawerLayout != null) {
            displayNavDrawer()
        } else {
            displayBottomNaView()
        }
    }

    private fun displayNavDrawer() {
        appBar = AppBarConfiguration(myNavController.graph, drawerLayout)
        NavigationUI.setupActionBarWithNavController(this, myNavController, appBar)
        NavigationUI.setupWithNavController(navView!!, myNavController)
    }

    private fun displayBottomNaView() {
        NavigationUI.setupActionBarWithNavController(this, myNavController)
        NavigationUI.setupWithNavController(bottomNavView!!, myNavController)
    }

    private fun promptBottomNewNote() {
        val prefManager = PreferenceManager.getDefaultSharedPreferences(this)

        if (!prefManager.getBoolean("didShowPrompt", false)) {
            MaterialTapTargetPrompt.Builder(this)
                .setTarget(R.id.newNoteFragment)
                .setPrimaryText(getString(R.string.new_note_prompt))
                .setSecondaryText(getString(R.string.new_note_description_prompt))
                .setBackButtonDismissEnabled(false)
                .setPromptBackground(RectanglePromptBackground())
                .setPromptFocal(RectanglePromptFocal())
                .setPromptStateChangeListener { _, state ->
                    if (state == MaterialTapTargetPrompt.STATE_FOCAL_PRESSED
                        || state == MaterialTapTargetPrompt.STATE_NON_FOCAL_PRESSED
                    ) {
                        val prefEditor = prefManager.edit()
                        prefEditor.putBoolean("didShowPrompt", true)
                        prefEditor.apply()

                        promptHowToUse()
                    }
                }
                .show()
        }
    }

    private fun promptHowToUse() {
        MaterialTapTargetPrompt.Builder(this)
            .setTarget(R.id.howToFragment)
            .setPrimaryText(getString(R.string.how_to_use_prompt))
            .setSecondaryText(getString(R.string.how_to_use_description_prompt))
            .setIcon(R.drawable.ic_help)
            .show()
    }

    override fun onSupportNavigateUp(): Boolean {
        // Hide virtual keyboard
        if (currentFocus != null) {
            val inputManager: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(
                currentFocus!!.windowToken,
                InputMethodManager.SHOW_FORCED
            )
        }
        return NavigationUI.navigateUp(myNavController, drawerLayout) or super.onSupportNavigateUp()
    }

    // Handle when bottom navigation selected
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item, myNavController)
                || super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
//        val layoutOrientation = resources.configuration.orientation
        if (null != drawerLayout && drawerLayout!!.isDrawerOpen(
                GravityCompat.START
            )
        ) {
            drawerLayout!!.closeDrawers()
        } else {
            super.onBackPressed()
        }
    }
}
