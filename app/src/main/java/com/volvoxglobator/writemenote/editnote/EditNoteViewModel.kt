package com.volvoxglobator.writemenote.editnote

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.volvoxglobator.writemenote.database.Note
import com.volvoxglobator.writemenote.database.NoteDatabaseDao
import kotlinx.coroutines.*
import org.threeten.bp.OffsetDateTime

class EditNoteViewModel(
    private val database: NoteDatabaseDao,
    application: Application
) : AndroidViewModel(application) {

    private val viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    // Update Note
    fun update(id: Long, title: String?, description: String) {
        uiScope.launch {
            if (!title.isNullOrBlank()) {
                val note = Note()
                note.noteId = id
                note.title = title
                note.description = description
                note.dateCreated = OffsetDateTime.now()

                withContext(Dispatchers.IO) {
                    database.update(note)
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}
