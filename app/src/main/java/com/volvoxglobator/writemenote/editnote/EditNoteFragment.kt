package com.volvoxglobator.writemenote.editnote

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.EditText
import androidx.activity.OnBackPressedCallback
import androidx.core.app.ShareCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.volvoxglobator.writemenote.R
import com.volvoxglobator.writemenote.database.NoteDatabase
import com.volvoxglobator.writemenote.databinding.FragmentEditNoteBinding
import com.volvoxglobator.writemenote.util.hideVirtualKeyBoard
import com.volvoxglobator.writemenote.util.showSuccessToast

class EditNoteFragment : Fragment() {


    private lateinit var editNoteviewModel: EditNoteViewModel
    private lateinit var binding: FragmentEditNoteBinding
    private lateinit var args: EditNoteFragmentArgs
    private var idContainer: Long = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_edit_note, container, false
        )

        val application = requireNotNull(this.activity).application
        val database = NoteDatabase.getInstance(this.requireContext()).noteDatabaseDao
        val viewModelFactory = EditNoteViewModelFactory(database, application)
        editNoteviewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(EditNoteViewModel::class.java)

        args = EditNoteFragmentArgs.fromBundle(arguments!!)
        idContainer = args.id
        binding.titleEdit.setText(args.titleNote)
        binding.descriptionEdit.setText(args.descriptionNote)

        binding.saveButton.setOnClickListener { v ->
            // Check if there is a changed from  a note (either title or description).
            // If there is, save updated note to database.
            validateData(
                binding.titleEdit, binding.descriptionEdit,
                args.titleNote, args.descriptionNote
            )
            hideVirtualKeyBoard(this.requireContext(), v)
        }

        setHasOptionsMenu(true)

        onBackPressed()

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.share_menu, menu)
        if (null == getShareIntent().resolveActivity(activity!!.packageManager)) {
            menu.findItem(R.id.share)?.setVisible(false)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.share -> shareANote()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun shareANote() {
        startActivity(Intent.createChooser(getShareIntent(), getString(R.string.share_note_string)))
    }

    private fun getShareIntent(): Intent {
        val args = EditNoteFragmentArgs.fromBundle(arguments!!)
        return ShareCompat.IntentBuilder.from(activity!!)
            .setText(getString(R.string.share_a_note, args.titleNote, args.descriptionNote))
            .setType("text/plain")
            .intent
    }

    private fun validateData(
        newTitle: EditText, description: EditText,
        oldTitle: String, oldDEscription: String
    ) {
        if (newTitle.text.isNullOrBlank()) {
            newTitle.error = getString(R.string.blank_title_error)
        } else if (!oldTitle.contentEquals(newTitle.text.toString()) ||
            !oldDEscription.contentEquals(description.text.toString())
        ) {
            editNoteviewModel.update(
                idContainer,
                newTitle.text.toString(),
                description.text.toString()
            )
            showSuccessToast(requireContext(), getString(R.string.edited_note_saved))
            navigate()
        } else {
            navigate()
        }
    }

    private fun navigate() {
        findNavController().navigate(
            EditNoteFragmentDirections.actionEditNoteFragmentToNoteListFragment()
        )
    }

    private fun onBackPressed() {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (!args.titleNote.contentEquals(binding.titleEdit.text.toString()) ||
                    !args.descriptionNote.contentEquals(binding.descriptionEdit.text.toString())
                ) {
                    val confirmationDialog = MaterialAlertDialogBuilder(requireContext())
                    confirmationDialog.apply {
                        setMessage(getString(R.string.confirmation_dialog_text))
                        setPositiveButton(getString(R.string.positive_button_dialog)) { _, _ ->
                            if (binding.titleEdit.text.isNullOrBlank()) {
                                binding.titleEditInput.error =
                                    getString(R.string.confirmation_dialog_text)
                            } else {
                                editNoteviewModel.update(
                                    args.id,
                                    binding.titleEdit.text.toString(),
                                    binding.descriptionEdit.text.toString()
                                )
                                showSuccessToast(
                                    requireContext(),
                                    getString(R.string.edited_note_saved)
                                )
                                navigate()
                            }
                        }
                        setNegativeButton(getString(R.string.negative_button_dialog)) { _, _ ->
                            navigate()
                        }
                        setNeutralButton(getString(R.string.neutral_button_dialog)) { dialog, _ ->
                            dialog.dismiss()
                        }

                        confirmationDialog.show()
                    }
                } else {
                    navigate()
                }
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }
}
