package com.volvoxglobator.writemenote.editnote

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.volvoxglobator.writemenote.database.NoteDatabaseDao

class EditNoteViewModelFactory(
    private val database: NoteDatabaseDao,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EditNoteViewModel::class.java)) {
            return EditNoteViewModel(database, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}