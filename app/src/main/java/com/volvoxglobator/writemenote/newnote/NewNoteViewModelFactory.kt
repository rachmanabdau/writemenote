package com.volvoxglobator.writemenote.newnote

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.volvoxglobator.writemenote.database.NoteDatabaseDao

class NewNoteViewModelFactory(
    private val database: NoteDatabaseDao,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NewNoteViewModel::class.java)) {
            return NewNoteViewModel(database, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}