package com.volvoxglobator.writemenote.newnote


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.volvoxglobator.writemenote.R
import com.volvoxglobator.writemenote.database.NoteDatabase
import com.volvoxglobator.writemenote.databinding.FragmentNewNoteBinding
import com.volvoxglobator.writemenote.util.hideVirtualKeyBoard
import com.volvoxglobator.writemenote.util.showSuccessToast

/**
 * A simple [Fragment] subclass.
 *
 */
class NewNoteFragment : Fragment() {

    private lateinit var binding: FragmentNewNoteBinding
    private lateinit var newNoteViewModel: NewNoteViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_new_note, container, false
        )

        val application = requireNotNull(this.activity).application
        val dataSource = NoteDatabase.getInstance(application).noteDatabaseDao
        val viewModelFactory = NewNoteViewModelFactory(dataSource, application)
        newNoteViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(NewNoteViewModel::class.java)

        binding.addNoteButton.setOnClickListener { v ->
            if (binding.titlenoteEdittext.text.isNullOrBlank()) {
                binding.titlenoteInputlayout.error = getString(R.string.blank_title_error)
            } else {
                newNoteViewModel.createNote(
                    binding.titlenoteEdittext.text.toString(),
                    binding.descriptionnoteEdittext.text.toString()
                )
                showSuccessToast(requireContext(), getString(R.string.new_note_created))
                navigate()
            }
            hideVirtualKeyBoard(this.requireContext(), v)
        }

        onBackPressed()

        return binding.root
    }

    private fun navigate() {
        findNavController()
            .navigate(NewNoteFragmentDirections.actionNewNoteFragmentToNoteListFragment())
    }

    private fun onBackPressed() {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (!binding.descriptionnoteEdittext.text.isNullOrBlank() ||
                    !binding.titlenoteEdittext.text.isNullOrBlank()
                ) {
                    val confirmationDialog = MaterialAlertDialogBuilder(requireContext())
                    confirmationDialog.apply {
                        setMessage(getString(R.string.confirmation_dialog_text))
                        setPositiveButton(getString(R.string.positive_button_dialog)) { _, _ ->
                            if (binding.titlenoteEdittext.text.isNullOrBlank()) {
                                binding.titlenoteInputlayout.error =
                                    getString(R.string.blank_title_error)
                            } else {
                                newNoteViewModel.createNote(
                                    binding.titlenoteEdittext.text.toString(),
                                    binding.descriptionnoteEdittext.text.toString()
                                )
                                showSuccessToast(
                                    requireContext(),
                                    getString(R.string.new_note_created)
                                )
                                navigate()
                            }
                        }
                        setNegativeButton(getString(R.string.negative_button_dialog)) { _, _ ->
                            navigate()
                        }
                        setNeutralButton(getString(R.string.neutral_button_dialog)) { dialog, _ ->
                            dialog.dismiss()
                        }
                    }
                    confirmationDialog.show()
                } else {
                    navigate()
                }
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }
}
