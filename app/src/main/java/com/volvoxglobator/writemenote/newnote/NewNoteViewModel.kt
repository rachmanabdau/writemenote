package com.volvoxglobator.writemenote.newnote

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.volvoxglobator.writemenote.database.Note
import com.volvoxglobator.writemenote.database.NoteDatabaseDao
import kotlinx.coroutines.*
import org.threeten.bp.OffsetDateTime

class NewNoteViewModel(val database: NoteDatabaseDao, application: Application) :
    AndroidViewModel(application) {

    private val viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    // Add a new note into database
    fun createNote(title: String, description: String?) {
        val newNote = Note()
        newNote.description = description ?: ""
        newNote.title = title
        newNote.dateCreated = OffsetDateTime.now()
        insert(newNote)
    }

    // Insert a new note in background thread
    private fun insert(note: Note) {
        uiScope.launch {
            withContext(Dispatchers.IO) {
                database.insert(note)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}