package com.volvoxglobator.writemenote.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface NoteDatabaseDao {

    @Insert
    fun insert(note: Note)

    @Update
    fun update(note: Note)

    @Delete
    fun delete(note: Note)

    @Query("SELECT * FROM note_table ORDER BY datetime(dateCreated) DESC")
    fun getAllNote(): LiveData<List<Note>>

    @Query("SELECT * FROM note_table WHERE noteId = :key")
    fun getANote(key: Long): Note?
}