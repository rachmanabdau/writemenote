# README.MD

### About This Application

Write Me Note is an application for creating simple note in android device. This application is inspired by Udacity course (developing andoid app with kotlin).

This app was created with.
* Framework MVVM & Life-cycle extension.
* Navigation pattern for navigating between fragment.
* SQLite database with Room.
* RecyclerView for displaying note list.

### Navigation Graph

![](/img/navigation_graph.png)

### First Time Run

![](/img/first_time_running.gif)

### Create A New Note

![](/img/create_a_new_note.gif)

### Edit A Note

![](/img/edit_a_note.gif)

### Delete A Note

![](/img/delete_a_note.gif)

### How To Use

![](/img/how_to_use_screen.png)

### APK File

Click [here](https://drive.google.com/file/d/1nQ7QM0MdJlJIH97pqw7mNeO7JDSaRe9l/view) to download installer.

### LICENSE

MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
